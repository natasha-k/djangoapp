#!/bin/bash
set -e

echo "Starting SSH ..."
/usr/sbin/sshd

printenv > testenv.txt
python /code/manage.py runserver 0.0.0.0:8000