FROM python:3.10-alpine3.15

RUN mkdir /code
WORKDIR /code
ADD requirements.txt /code/
RUN pip install -r requirements.txt
ADD . /code/

# ssh
ENV SSH_PASSWD "root:Docker!"
RUN apk add openssh \
     && echo "$SSH_PASSWD" | chpasswd

COPY sshd_config /etc/ssh/
# Copy and configure the ssh_setup.sh file
COPY ssh_setup.sh /etc/ssh/ssh_setup.sh
RUN chmod -R +x /etc/ssh/ssh_setup.sh; \
   (sleep 1;. /etc/ssh/ssh_setup.sh 2>&1 > /dev/null); \
   rm -rf /etc/ssh/ssh_setup.sh

COPY init.sh /usr/local/bin/

RUN chmod u+x /usr/local/bin/init.sh
EXPOSE 8000 2222
#CMD ["python", "/code/manage.py", "runserver", "0.0.0.0:8000"]
ENTRYPOINT ["/bin/sh", "init.sh"]